/**
* @main.c
* @brief 
*
*This program accepts two inputs from user and search for the 8 or 16 bit
pattern.
*
*
* 
* @note
*
* Revision History:
* - 220921  DAM : Creation Date
*/

#include <stdio.h>
#include "patternsearch.h"
#include <string.h>
#include <stdlib.h>

/*******************************************
* Const and Macro Defines
*******************************************/

#define ARGC_REQUIRED 3
#define ARGC_MINIMUM 1

/***********************************
* Typedefs and Enum Declarations
***********************************/

// none

/***********************************
* External Variable Declarations
***********************************/

// none

/***********************************
* Const Declarations
***********************************/

// none

/***********************************
* Public Variables
***********************************/

// none

/***********************************
* Private Variables
***********************************/

// none

/***********************************
* Private Prototypes
***********************************/

void manual(void);

/***********************************
* Public Functions
***********************************/

/**
* @manual
* @brief 
*
*This function displys the help option for user
*
*
* 
* @note
*
* Revision History:
* - 240921  DAM : Creation Date
*/

void manual(void){
	char quit;	
	printf("\e[1;1H\e[2J");	
	printf("NAME");
	printf("\n	ps	-	Pattern Search\n");
	printf("\nSynopsis");
	printf("\n	ps [PATTERN] [DIRECTORY]\n");
	printf("\n [PATTERN] - 1111 or 11 [0x88 or 0xABAB - Not Allowed]");
	printf("\n");
	printf("\nDescription");
	printf("\n	ps searches for a 8bit or 16bit pattern entered by");
	printf(" the user on the binary file which is specified\n");
	printf("\nExamples");
	printf("\n	ps 1A1A /home/desktop/test.bin");
	printf("\n	ps 13 /home/desktop/test.bin\n");
	printf("\n\n\nPress \"Q\" to Quit\n\n");
	scanf(" %c",&quit);
}
	



int main(int argc,char *argv[])
{
	if(argc==ARGC_REQUIRED){
		int pattern_len;
		FILE *fileptr;
		char *temp;
	
		temp = argv[1];
		fileptr = fopen(argv[2],"rb+");
		pattern_len = strlen(argv[1]);
		if(pattern_len>4||pattern_len==3){
			printf("Invalid Pattern");
			printf("\nTry 'ps -h'for information");
			return 0;
		}
		for(int itr=0;itr<pattern_len;itr++){
			if(((temp[itr]>64 && temp[itr]<71)||(temp[itr]>96&&temp[itr]<103)||(temp[itr]>47&&temp[itr]<58))==0){
				printf("Invalid Search Pattern");
   				printf("\nTry 'ps -h' for more information\n");
				return 0;
			}	
		}
		if(fileptr!=NULL){
		/*	Invoking patternsearch function */
			patternsearch(fileptr,argv[1],pattern_len);		
			} else{
			printf("Error:File not found");
			}
	}else if(argc==ARGC_MINIMUM){	
		printf("ps : missing file operand");
   		printf("\nTry 'ps -h' for more information\n");
	}else {
		if(!strcmp(argv[1],"-h")){
		manual();
		return 0;
		}
		printf("ps : missing file operand");
   		printf("\nTry 'ps -h' for more information\n");
	}
	return 0;
}
