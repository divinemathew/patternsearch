/**
* @fileoperation.c
* @brief 
*
* All file operations are done in this file
*
* @para
* 
* @note
*
* Revision History:
* - 220921  DAM : Creation Date
*/

#include <stdio.h>
#include "patternsearch.h"
#include <stdlib.h>

/*******************************************
* Const and Macro Defines
*******************************************/

#define PATTERN_LEN_MAX 4
#define PATTERN_LEN_MIN 2


/***********************************
* Typedefs and Enum Declarations
***********************************/

// none

/***********************************
* External Variable Declarations
***********************************/

// none

/***********************************
* Const Declarations
***********************************/

// none

/***********************************
* Public Variables
***********************************/

// none

/***********************************
* Private Variables
***********************************/

// none

/***********************************
* Private Prototypes
***********************************/

// none

/***********************************
* Public Functions
***********************************/

/**
* @patternsearch
* @brief 
*
* This function searches for the 8/16 bit pattern given by the user.
*
* @param	*fptr		:	file pointer to the binary file
* @param	pattern[]	:	pattern which the user is inputting
* @param	pattern_len	:	length of pattern
* @return	int	
*
*
* @note
*
* Revision History:
* - 220921  DAM : Creation Date
*/
int patternsearch(FILE *fptr, char pattern[],int pattern_len)
{	
	char *temp;
	char pattern_lsb[2];
	char pattern_msb[2];
	int *offsetvalue;
	//int offsetvalue[MAXVALUE];
	int counter=0;
	int offset=0;
	int str_read;
	offsetvalue = (int*)calloc(1, sizeof(int));

/*For 8 bit pattern*/
	if(pattern_len == PATTERN_LEN_MIN){
		long pattern_hex =  strtol(pattern,&temp,16);
		while(feof(fptr)!=1){
			++offset;
			if(offset>=0){
			offsetvalue = (int*)realloc(offsetvalue, sizeof(int)*offset);
			}str_read = getc(fptr);
			if(str_read == pattern_hex){
				counter++;
				offsetvalue[counter]=offset;
			}
		}	
/* For 16 bit */
	} else if(pattern_len == PATTERN_LEN_MAX){
		pattern_msb[0] = pattern[0];
		pattern_msb[1] = pattern[1];

		long pattern_hex_msb = strtol(pattern_msb,&temp,16);
		
		while(feof(fptr)!=1){
			offset++;
			str_read = getc(fptr);
			if(str_read==pattern_hex_msb){
				pattern_lsb[0] = pattern[2];
				pattern_lsb[1] = pattern[3];
				pattern_lsb[2] = 0;

				long pattern_hex_lsb = strtol(pattern_lsb,&temp,16);
				str_read = getc(fptr);
				if(str_read==pattern_hex_lsb){
					counter++;
					offsetvalue = (int*)realloc(offsetvalue, sizeof(int)*offset);
					offsetvalue[counter]=offset;
					fseek(fptr,-1,SEEK_CUR);
				}
			}
		}
		
	}	
	printf("Pattern Searched	:	0x%s",pattern);
	printf("\nMatches Found		:	%d",counter);
	printf("\nOffset Locations	:	");
	for(int i=1;i<=counter;i++){
	printf("%d",offsetvalue[i]-1);
	if(i!=counter)
		printf(", ");
	}
	printf("\n");
	fclose(fptr);	
	free(offsetvalue);
	return 0;
}
